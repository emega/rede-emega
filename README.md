# Rede emega

## Proxecto
Web realizada no curso https://emegasxxi.com/

## Uso básico de git
Repositorio: https://gitlab.com/emega/rede-emega/
### Clonar o proxecto
```shell
git clone git@gitlab.com:emega/rede-emega.git
```
*So traballaremos con unha rama (main)

### Actualizar o proxecto
Úsase para descargar últimos cambios
```shell
git pull
```
### Para facer un commit
Os commits son locais
```shell
git add .
git commit -m "mensaxe coa descrición dos cambios"
```
### Para facer un push
Con push subimos os nosos cambios ao servidor
```shell
git push
```
* Pode dar conflictos!




